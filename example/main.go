package main

import (
	helpers "bitbucket.org/testbookdotcom/redigo-wrapper"
	"fmt"
	redigo "github.com/gomodule/redigo/redis"
)

func main() {

	//initialize a redis connection pool
	RPool := helpers.NewRConnectionPool(
		helpers.Config{
			Server:              "localhost:6379",
			Password:            "",
			KEY_PREFIX:          "development",
			KEY_DELIMITER:       ":",
			KEY_VAR_PLACEHOLDER: "?",
		},
	)

	// get hold of connection object from the pool
	RConnection := RPool.Get()
	defer RConnection.Close()
	RConn := (&RConnection)
	TestZSET(RConn)

}

func GetM(RConn *redigo.Conn, key []string, result *[]string) error {
	data, err := helpers.MGet(RConn, key)
	if err != nil {
		return err
	}
	*result, err = redigo.Strings(data, err)
	return err
}

func TestZSET(RConn *redigo.Conn) {
	//Add single element
	key := "check"
	_, err := helpers.ZAdd(RConn, key, 0, "lms")
	if err != nil {
		fmt.Println("Error in zadd, Error : ", err.Error())
	}

	//Add multiple element
	_, err = helpers.ZAddRange(RConn, key, 0, "lms1", 1, "lms2", 2, "lms3")
	if err != nil {
		fmt.Println("Error in zaddrange, Error : ", err.Error())
	}

	//Count
	countResult, err := helpers.ZCard(RConn, key)
	if err != nil {
		fmt.Println("Error in zcard, Error : ", err.Error())
	}
	fmt.Println("Count : ", countResult)

	//Members in a z-set
	members, err := helpers.ZRangeString(RConn, key, 0, -1, true)
	if err != nil {
		fmt.Println("Error in zrange, Error : ", err.Error())
	}
	fmt.Println("Members : ", members)

}
